from django.db import models
# Create your models here.

class User(models.Model):
    User_ID = models.AutoField(primary_key=True)
    User_name = models.CharField(max_length=25)
    User_password = models.CharField(max_length=25)
    User_mod = models.BooleanField(default=False)
    def __str__(self):
        return str(self.User_ID)

class Note(models.Model):
    Note_ID = models.AutoField(primary_key=True)
    Note_name = models.CharField(max_length=25)
    Note_user = models.ForeignKey(User, on_delete=models.CASCADE)
    Note_text = models.TextField(max_length=1000)
    def __str__(self):
        return self.Note_name
    
