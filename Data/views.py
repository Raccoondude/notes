from django.shortcuts import render
from .models import *
from .forms import *
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.

def index(request):
    ID = request.session['ID']
    if ID == '':
        return render(request, 'Data/index2.html')
    else:
        try:
            OwO = User.objects.get(User_ID=str(ID))
        except User.DoesNotExist:
            return render(request, 'Data/index2.html')
        return render(request, 'Data/index.html', {'OwO':OwO})

def signup(request):
    form = CreateForm()
    return render(request, 'Data/signup.html', {'form':form})

def create(request):
    if request.method == 'POST':
        MyForm = CreateForm(request.POST)
        if MyForm.is_valid():
            username = MyForm.cleaned_data['Username']
            password = MyForm.cleaned_data['Password']
            OwO = User(User_name=username, User_password=password)
            OwO.save()
            return render(request, 'Data/done.html', {'OwO':OwO})
        else:
            return HttpResponse("Not valid")
    else:
        return HttpResponseRedirect("/")

def login(request):
    form = LoginForm()
    return render(request, 'Data/login.html', {'form':form})

def log(request):
    if request.method == 'POST':
        MyForm = LoginForm(request.POST)
        if MyForm.is_valid():
            UwU = MyForm.cleaned_data['ID']
            password = MyForm.cleaned_data['Password']
            try:
                OwO = User.objects.get(User_ID=UwU)
            except User.DoesNotExist:
                return HttpResponse("Wrong ID <a href='/login/'>Go back</a>")
            if OwO.User_password == password:
                request.session['ID'] = OwO.User_ID
                return HttpResponseRedirect('/')
            else:
                return HttpResponse("Wrong password <a href='/login/'>Go back</a>")
        else:
            return HttpResponse("error")
    else:
        return HttpResponeRedirect('/')

def logout(request):
    request.session['ID'] = ''
    return HttpResponseRedirect('/')

def make_note(request):
    ID = request.session['ID']
    if ID == '':
        return HttpResponseRediret('/login/')
    else:
        try:
            OwO = User.objects.get(User_ID=ID)
        except User.DoesNotExist:
            return HttpResponseRedirect('/login/')
        form = NoteForm()
        return render(request, 'Data/notes.html', {'form':form})

def notes(request):
    if request.method == "POST":
        MyForm = NoteForm(request.POST)
        if MyForm.is_valid():
            Text = MyForm.cleaned_data['Note']
            ID = request.session['ID']
            Name = MyForm.cleaned_data['Name']
            OwO = User.objects.get(User_ID=ID)
            UwU = Note(Note_user=OwO, Note_text=Text, Note_name=Name)
            UwU.save()
            return HttpResponse("Finished, <a href='/'>Go back</a>")
        else:
            return HttpResponse("error, <a href='/'>Go back</a>")
    else:
        return HttpResponseRedirect("/")

def view_notes(request):
    ID = request.session['ID']
    if ID == '':
        return HttpResponseRedirect('/')
    else:
        try:
            OwO = User.objects.get(User_ID=ID)
        except User.DoesNotExist:
            return HttpResponseRedirect('/')
        note_list = Note.objects.filter(Note_user=OwO)
        return render(request, 'Data/views.html', {'note_list':note_list, 'OwO':OwO})

def mod(request):
    ID = request.session['ID']
    if ID == '':
        return HttpResponse("Your not supposed to be here, <a href='/'>Go back</a>")
    try:
        OwO = User.objects.get(User_ID=ID)
    except User.DoesNotExist:
        return HttpResponse('Your not supposed to be here, <a href"/">Go back</a>')
    if OwO.User_mod == True:
        form = ModForm()
        return render(request, "Data/mod.html", {'form':form})
    else:
        return HttpResponse("You are not mod")

def mods(request):
    if request.method == 'POST':
        MyForm = ModForm(request.POST)
        if MyForm.is_valid():
            ID = MyForm.cleaned_data['user']
            password = MyForm.cleaned_data['password']
            my_ID = request.session['ID']
            try:
                OwO = User.objects.get(User_ID=my_ID)
            except User.DoesNotExist:
                return HttpResponse("error")
            if OwO.User_mod == False:
                return HttpResponse('error')
            else:
                try:
                    UwU = User.objects.get(User_ID=ID)
                except User.DoesNotExist:
                    return HttpResponse("error")
                Name = UwU.User_name
                if UwU.User_mod == True:
                    return HttpResponse("You cannot delete another mod")
                if password != OwO.User_password:
                    return HttpResponse("error")
                else:
                    UwU.delete()
                    return HttpResponse("Deleted  " + Name + " <a href='/'>Go back</a>")
        else:
            return HttpResponse("You are not mod")
