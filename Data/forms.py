from django import forms

class CreateForm(forms.Form):
    Username = forms.CharField(max_length=25)
    Password = forms.CharField(max_length=25)

class LoginForm(forms.Form):
    ID = forms.IntegerField()
    Password = forms.CharField(max_length=25)

class NoteForm(forms.Form):
    Note = forms.CharField(widget=forms.Textarea)
    Name = forms.CharField(max_length=25)

class ModForm(forms.Form):
    user = forms.IntegerField()
    password = forms.CharField(max_length=25)
