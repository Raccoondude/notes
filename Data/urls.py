from django.urls import path

from . import views

urlpatterns =[
    path('', views.index, name='index'),
    path("signup/", views.signup, name='sign up'),
    path('create/', views.create, name='create'),
    path('login/', views.login, name='login'),
    path('log/', views.log, name='log'),
    path('logout/', views.logout, name='logout'),
    path('note/', views.make_note, name='note'),
    path('notes/', views.notes, name='notes'),
    path('view/', views.view_notes, name='view notes'),
    path('mod/', views.mod, name="mod"),
    path("mods/", views.mods, name="mods"),
]
